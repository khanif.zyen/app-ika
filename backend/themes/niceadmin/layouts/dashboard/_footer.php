 <!-- ======= Footer ======= -->
 <footer id="footer" class="footer">
     <div class="copyright">
         &copy; Copyright <strong><span>Divisi II IKA SMANSARA</span></strong>. All Rights Reserved
     </div>
     <div class="credits">
         Designed by <a href="https://khanifzyen.com/">AK.Zyen</a>
     </div>
 </footer><!-- End Footer -->