<?php

use backend\widgets\MyMenu;

?>
<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="index.html">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <?php /*echo MyMenu::widget([
            'options' => ['class' => 'sidebar-nav', 'id' => 'sidebar-nav'],
            'linkTemplate' => '<a href="{url}" class="nav-link">{label}</a>',
            'submenuTemplate' => '\n<ul class="nav-content collapse" data-bs-parent="#sidebar-nav">\n{items}\n</ul>\n',
            'encodeLabels' => false,
            'items' => [
                // Important: you need to specify url as 'controller/action',
                // not just as 'controller' even if default action is used.
                ['label' => '<i class="bi bi-grid"></i>Dash', 'url' => ['site/index']],
                // 'Products' menu item will be selected as long as the route is 'product/index'
                ['label' => '<i class="bi bi-person"></i>Data Alumni<i class="bi bi-chevron-down ms-auto"></i>', 'url' => ['#'], 'items' => [
                    ['label' => '<i class="bi bi-arrow-right-circle"></i>Semua Alumni', 'url' => ['product/index', 'tag' => 'new']],
                    ['label' => '<i class="bi bi-arrow-right-circle"></i>Tambah Alumni', 'url' => ['product/index', 'tag' => 'popular']],
                ]],
                ['label' => '<i class="bi bi-person"></i>Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
            ],
        ]); */ ?>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#member-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-people"></i><span>Member</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="member-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                <li>
                    <a href="components-alerts.html">
                        <i class="bi bi-circle"></i><span>Semua Member</span>
                    </a>
                </li>
                <a href="components-alerts.html">
                    <i class="bi bi-circle"></i><span>Tambah Member</span>
                </a>
        </li>

    </ul>
    </li><!-- End Member Nav -->

    <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tiket-nav" data-bs-toggle="collapse" href="#">
            <i class="bi bi-ticket"></i><span>Tiket</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tiket-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
            <li>
            <li>
                <a href="components-alerts.html">
                    <i class="bi bi-circle"></i><span>Semua Tiket</span>
                </a>
            </li>
            <a href="components-alerts.html">
                <i class="bi bi-circle"></i><span>Tambah Tiket</span>
            </a>
    </li>

    </ul>
    </li><!-- End Tiket Nav -->

    <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#shop-nav" data-bs-toggle="collapse" href="#">
            <i class="bi bi-shop"></i><span>Belanja</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="shop-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
            <li>
            <li>
                <a href="components-alerts.html">
                    <i class="bi bi-circle"></i><span>Semua Produk</span>
                </a>
            </li>
            <a href="components-alerts.html">
                <i class="bi bi-circle"></i><span>Tambah Produk</span>
            </a>
    </li>

    </ul>
    </li><!-- End Tiket Nav -->

    </ul>

</aside><!-- End Sidebar-->