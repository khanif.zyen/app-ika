    <!-- ======= Header ======= -->
    <header id="header" class="header fixed-top d-flex align-items-center">

        <?= $this->render('header/_logo') ?>
        <?= $this->render('header/_searchbar') ?>

        <nav class="header-nav ms-auto">
            <ul class="d-flex align-items-center">

                <li class="nav-item d-block d-lg-none">
                    <a class="nav-link nav-icon search-bar-toggle " href="#">
                        <i class="bi bi-search"></i>
                    </a>
                </li><!-- End Search Icon-->

                <?= $this->render('header/_notifications') ?>

                <?= $this->render('header/_message') ?>

                <?= $this->render('header/_profile') ?>

            </ul>
        </nav><!-- End Icons Navigation -->

    </header><!-- End Header -->