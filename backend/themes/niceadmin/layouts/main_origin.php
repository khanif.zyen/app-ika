<?php

/** @var \yii\web\View $this */
/** @var string $content */

use backend\assets\AppAsset;
use backend\assets\NiceAdminAsset;
use common\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);
NiceAdminAsset::register($this);

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->registerMetaTag(['name' => 'keywords', 'content' => 'ika, aplikasi ika, ikatan alumni, sman1 jepara, reuni akbar, aplikasi reuni, pemesanaan tiket']); ?>
    <?php $this->registerMetaTag(['name' => 'description', 'content' => 'Aplikasi IKA merupakan aplikasi all in one untuk pendataan ikatan alumni SMAN 1 Jepara dan untuk pendaftaran event seperti Reuni Akbar dan penjualan marchandise']); ?>
    <title>Dashboard - Aplikasi IKA</title>
    <?php
    // Favicon
    $this->registerLinkTag([
        'rel' => 'icon',
        'href' => 'themes/niceadmin/img/favicon.png',
    ]);
    $this->registerLinkTag([
        'rel' => 'apple-touch-icon',
        'href' => 'themes/niceadmin/img/apple-touch-icon.png',
    ]);
    // Google Fonts
    $this->registerLinkTag([
        'rel' => 'preconnect',
        'href' => 'https://fonts.gstatic.com',
    ]);
    $this->registerLinkTag([
        'rel' => 'stylesheet',
        'href' => 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i',
    ]);
    ?>

    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <?= $this->render('_header'); ?>

    <?= $this->render('_sidebar'); ?>

    <main id="main" class="main">

        <div class="pagetitle">
            <h1><?= $this->title ?></h1>

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

        </div><!-- End Page Title -->
        <?= Alert::widget() ?>
        <?= $content ?>
        <section class="section dashboard">
            <div class="row">

                <!-- Left side columns -->
                <div class="col-lg-8">
                    <div class="row">

                        <?= $this->render('_salesCard') ?>

                        <?= $this->render('_revenueCard') ?>

                        <?= $this->render('_customerCard') ?>

                        <?= $this->render('_reports') ?>

                        <?= $this->render('_recentSales') ?>

                        <?= $this->render('_topSelling') ?>

                    </div>
                </div><!-- End Left side columns -->

                <!-- Right side columns -->
                <div class="col-lg-4">

                    <?= $this->render('_recentActivity') ?>

                    <?= $this->render('_budgetReport') ?>

                    <?= $this->render('_websiteTraffic') ?>

                    <?= $this->render('_newsAndUpdateTraffic') ?>

                </div><!-- End Right side columns -->

            </div>
        </section>

    </main><!-- End #main -->

    <?= $this->render('_footer') ?>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage();
