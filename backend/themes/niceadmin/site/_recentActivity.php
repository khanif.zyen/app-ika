<!-- Recent Activity -->
<div class="card">
    <div class="filter">
        <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
            <li class="dropdown-header text-start">
                <h6>Filter</h6>
            </li>

            <li><a class="dropdown-item" href="#">Today</a></li>
            <li><a class="dropdown-item" href="#">This Month</a></li>
            <li><a class="dropdown-item" href="#">This Year</a></li>
        </ul>
    </div>

    <div class="card-body">
        <h5 class="card-title">Recent Activity <span>| Today</span></h5>

        <div class="activity">

            <div class="activity-item d-flex">
                <div class="activite-label">32 min</div>
                <i class='bi bi-circle-fill activity-badge text-success align-self-start'></i>
                <div class="activity-content">
                    User <a href="#" class="fw-bold text-dark">khanif</a> membayar 5 <a href="#" class="fw-bold text-dark">tiket reuni akbar 2022</a>
                </div>
            </div><!-- End activity item-->

            <div class="activity-item d-flex">
                <div class="activite-label">56 min</div>
                <i class='bi bi-circle-fill activity-badge text-danger align-self-start'></i>
                <div class="activity-content">
                    User <a href="#" class="fw-bold text-dark">khanif</a> memesan 5 <a href="#" class="fw-bold text-dark">tiket reuni akbar 2022</a>
                </div>
            </div><!-- End activity item-->

            <div class="activity-item d-flex">
                <div class="activite-label">1 hrs</div>
                <i class='bi bi-circle-fill activity-badge text-muted align-self-start'></i>
                <div class="activity-content">
                    User <a href="#" class="fw-bold text-dark">khanif</a> membayar 1 <a href="#" class="fw-bold text-dark">Official Merchandise Reuni Akbar 2022</a>
                </div>
            </div><!-- End activity item-->

            <div class="activity-item d-flex">
                <div class="activite-label">2 hrs</div>
                <i class='bi bi-circle-fill activity-badge text-primary align-self-start'></i>
                <div class="activity-content">
                    User <a href="#" class="fw-bold text-dark">khanif</a> memesan 1 <a href="#" class="fw-bold text-dark">Official Merchandise Reuni Akbar 2022</a>
                </div>
            </div><!-- End activity item-->

            <div class="activity-item d-flex">
                <div class="activite-label">1 day</div>
                <i class='bi bi-circle-fill activity-badge text-info align-self-start'></i>
                <div class="activity-content">
                    Status pemesanan <a href="#" class="fw-bold text-dark">Official Merchandise Reuni Akbar 2022</a> user <a href="#" class="fw-bold text-dark">khanif</a> batal
                </div>
            </div><!-- End activity item-->

            <div class="activity-item d-flex">
                <div class="activite-label">2 days</div>
                <i class='bi bi-circle-fill activity-badge text-warning align-self-start'></i>
                <div class="activity-content">
                    User <a href="#" class="fw-bold text-dark">khanif</a> memesan 1 <a href="#" class="fw-bold text-dark">Official Merchandise Reuni Akbar 2022</a>
                </div>
            </div><!-- End activity item-->



        </div>

    </div>
</div><!-- End Recent Activity -->