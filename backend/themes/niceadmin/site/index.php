<?php
$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section dashboard">
    <div class="row">

        <!-- Left side columns -->
        <div class="col-lg-8">
            <div class="row">

                <?= $this->render('_alumniCard'); ?>
                <?= $this->render('_ticketCard'); ?>
                <?= $this->render('_merchandiseCard'); ?>
                <?= $this->render('_reports') ?>
                <?= $this->render('_recentSales') ?>



            </div>
        </div><!-- End Left side columns -->

        <!-- Right side columns -->
        <div class="col-lg-4">

            <?= $this->render('_recentActivity') ?>
            <?= $this->render('_newsAndUpdateTraffic') ?>

        </div><!-- End Right side columns -->

    </div>
</section>