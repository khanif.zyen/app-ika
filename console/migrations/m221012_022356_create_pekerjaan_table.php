<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pekerjaan}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_022356_create_pekerjaan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pekerjaan}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-pekerjaan-created_by}}',
            '{{%pekerjaan}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-pekerjaan-created_by}}',
            '{{%pekerjaan}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-pekerjaan-updated_by}}',
            '{{%pekerjaan}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-pekerjaan-updated_by}}',
            '{{%pekerjaan}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-pekerjaan-created_by}}',
            '{{%pekerjaan}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-pekerjaan-created_by}}',
            '{{%pekerjaan}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-pekerjaan-updated_by}}',
            '{{%pekerjaan}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-pekerjaan-updated_by}}',
            '{{%pekerjaan}}'
        );

        $this->dropTable('{{%pekerjaan}}');
    }
}
