<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%event_panitia}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_065452_create_event_panitia_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%event_panitia}}', [
            'id' => $this->primaryKey(),
            'id_event' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_event`
        $this->createIndex(
            '{{%idx-event_panitia-id_event}}',
            '{{%event_panitia}}',
            'id_event'
        );

        // add foreign key for table `{{%event}}`
        $this->addForeignKey(
            '{{%fk-event_panitia-id_event}}',
            '{{%event_panitia}}',
            'id_event',
            '{{%event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_user`
        $this->createIndex(
            '{{%idx-event_panitia-id_user}}',
            '{{%event_panitia}}',
            'id_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_panitia-id_user}}',
            '{{%event_panitia}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-event_panitia-created_by}}',
            '{{%event_panitia}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_panitia-created_by}}',
            '{{%event_panitia}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-event_panitia-updated_by}}',
            '{{%event_panitia}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_panitia-updated_by}}',
            '{{%event_panitia}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%event}}`
        $this->dropForeignKey(
            '{{%fk-event_panitia-id_event}}',
            '{{%event_panitia}}'
        );

        // drops index for column `id_event`
        $this->dropIndex(
            '{{%idx-event_panitia-id_event}}',
            '{{%event_panitia}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_panitia-id_user}}',
            '{{%event_panitia}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            '{{%idx-event_panitia-id_user}}',
            '{{%event_panitia}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_panitia-created_by}}',
            '{{%event_panitia}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-event_panitia-created_by}}',
            '{{%event_panitia}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_panitia-updated_by}}',
            '{{%event_panitia}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-event_panitia-updated_by}}',
            '{{%event_panitia}}'
        );

        $this->dropTable('{{%event_panitia}}');
    }
}
