<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%event}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_025141_create_event_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'owner' => $this->integer()->notNull(),
            'nama' => $this->string()->notNull(),
            'deskripsi' => $this->text(),
            'tanggal_mulai' => $this->dateTime()->notNull(),
            'tanggal_selesai' => $this->dateTime()->notNull(),
            'event_seharian' => $this->boolean()->defaultValue(0),
            'tempat' => $this->string()->notNull(),
            'alamat' => $this->string(),
            'foto' => $this->string(),
            'total_tiket' => $this->integer(),
            'tiket_unlimited' => $this->boolean()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `owner`
        $this->createIndex(
            '{{%idx-event-owner}}',
            '{{%event}}',
            'owner'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event-owner}}',
            '{{%event}}',
            'owner',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-event-created_by}}',
            '{{%event}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event-created_by}}',
            '{{%event}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-event-updated_by}}',
            '{{%event}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event-updated_by}}',
            '{{%event}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event-owner}}',
            '{{%event}}'
        );

        // drops index for column `owner`
        $this->dropIndex(
            '{{%idx-event-owner}}',
            '{{%event}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event-created_by}}',
            '{{%event}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-event-created_by}}',
            '{{%event}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event-updated_by}}',
            '{{%event}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-event-updated_by}}',
            '{{%event}}'
        );

        $this->dropTable('{{%event}}');
    }
}
