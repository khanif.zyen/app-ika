<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%variasi_nilai}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%variasi}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_075615_create_variasi_nilai_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%variasi_nilai}}', [
            'id' => $this->primaryKey(),
            'id_variasi' => $this->integer()->notNull(),
            'nilai' => $this->string()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_variasi`
        $this->createIndex(
            '{{%idx-variasi_nilai-id_variasi}}',
            '{{%variasi_nilai}}',
            'id_variasi'
        );

        // add foreign key for table `{{%variasi}}`
        $this->addForeignKey(
            '{{%fk-variasi_nilai-id_variasi}}',
            '{{%variasi_nilai}}',
            'id_variasi',
            '{{%variasi}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-variasi_nilai-created_by}}',
            '{{%variasi_nilai}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-variasi_nilai-created_by}}',
            '{{%variasi_nilai}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-variasi_nilai-updated_by}}',
            '{{%variasi_nilai}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-variasi_nilai-updated_by}}',
            '{{%variasi_nilai}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%variasi}}`
        $this->dropForeignKey(
            '{{%fk-variasi_nilai-id_variasi}}',
            '{{%variasi_nilai}}'
        );

        // drops index for column `id_variasi`
        $this->dropIndex(
            '{{%idx-variasi_nilai-id_variasi}}',
            '{{%variasi_nilai}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-variasi_nilai-created_by}}',
            '{{%variasi_nilai}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-variasi_nilai-created_by}}',
            '{{%variasi_nilai}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-variasi_nilai-updated_by}}',
            '{{%variasi_nilai}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-variasi_nilai-updated_by}}',
            '{{%variasi_nilai}}'
        );

        $this->dropTable('{{%variasi_nilai}}');
    }
}
