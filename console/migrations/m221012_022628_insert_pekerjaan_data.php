<?php

use yii\db\Migration;

/**
 * Class m221012_022628_insert_pekerjaan_data
 */
class m221012_022628_insert_pekerjaan_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%pekerjaan}}', ['nama', 'created_by', 'updated_by', 'created_at', 'updated_at'], [
            ['Wiraswasta', 1, 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')],
            ['TNI/Polri', 1, 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')],
            ['Pegawai Negeri Sipil', 1, 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')],
            ['Pegawai Swasta', 1, 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')],
            ['Ibu Rumah Tangga', 1, 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221012_022628_insert_pekerjaan_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221012_022628_insert_pekerjaan_data cannot be reverted.\n";

        return false;
    }
    */
}
