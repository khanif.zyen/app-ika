<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%variasi}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_075023_create_variasi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%variasi}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-variasi-created_by}}',
            '{{%variasi}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-variasi-created_by}}',
            '{{%variasi}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-variasi-updated_by}}',
            '{{%variasi}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-variasi-updated_by}}',
            '{{%variasi}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-variasi-created_by}}',
            '{{%variasi}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-variasi-created_by}}',
            '{{%variasi}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-variasi-updated_by}}',
            '{{%variasi}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-variasi-updated_by}}',
            '{{%variasi}}'
        );

        $this->dropTable('{{%variasi}}');
    }
}
