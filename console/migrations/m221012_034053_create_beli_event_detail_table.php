<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%beli_event_detail}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%beli_event}}`
 * - `{{%tiket_kategori_harga}}`
 * - `{{%event_coupon}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_034053_create_beli_event_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%beli_event_detail}}', [
            'id' => $this->primaryKey(),
            'id_beli_event' => $this->integer()->notNull(),
            'id_tiket_kategori_harga' => $this->integer()->notNull(),
            'id_event_coupon' => $this->integer(),
            'harga' => $this->bigInteger()->notNull(),
            'jumlah_tiket' => $this->integer()->notNull(),
            'total_harga' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_beli_event`
        $this->createIndex(
            '{{%idx-beli_event_detail-id_beli_event}}',
            '{{%beli_event_detail}}',
            'id_beli_event'
        );

        // add foreign key for table `{{%beli_event}}`
        $this->addForeignKey(
            '{{%fk-beli_event_detail-id_beli_event}}',
            '{{%beli_event_detail}}',
            'id_beli_event',
            '{{%beli_event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_tiket_kategori_harga`
        $this->createIndex(
            '{{%idx-beli_event_detail-id_tiket_kategori_harga}}',
            '{{%beli_event_detail}}',
            'id_tiket_kategori_harga'
        );

        // add foreign key for table `{{%tiket_kategori_harga}}`
        $this->addForeignKey(
            '{{%fk-beli_event_detail-id_tiket_kategori_harga}}',
            '{{%beli_event_detail}}',
            'id_tiket_kategori_harga',
            '{{%tiket_kategori_harga}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_event_coupon`
        $this->createIndex(
            '{{%idx-beli_event_detail-id_event_coupon}}',
            '{{%beli_event_detail}}',
            'id_event_coupon'
        );

        // add foreign key for table `{{%event_coupon}}`
        $this->addForeignKey(
            '{{%fk-beli_event_detail-id_event_coupon}}',
            '{{%beli_event_detail}}',
            'id_event_coupon',
            '{{%event_coupon}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-beli_event_detail-created_by}}',
            '{{%beli_event_detail}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_event_detail-created_by}}',
            '{{%beli_event_detail}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-beli_event_detail-updated_by}}',
            '{{%beli_event_detail}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_event_detail-updated_by}}',
            '{{%beli_event_detail}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%beli_event}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_detail-id_beli_event}}',
            '{{%beli_event_detail}}'
        );

        // drops index for column `id_beli_event`
        $this->dropIndex(
            '{{%idx-beli_event_detail-id_beli_event}}',
            '{{%beli_event_detail}}'
        );

        // drops foreign key for table `{{%tiket_kategori_harga}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_detail-id_tiket_kategori_harga}}',
            '{{%beli_event_detail}}'
        );

        // drops index for column `id_tiket_kategori_harga`
        $this->dropIndex(
            '{{%idx-beli_event_detail-id_tiket_kategori_harga}}',
            '{{%beli_event_detail}}'
        );

        // drops foreign key for table `{{%event_coupon}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_detail-id_event_coupon}}',
            '{{%beli_event_detail}}'
        );

        // drops index for column `id_event_coupon`
        $this->dropIndex(
            '{{%idx-beli_event_detail-id_event_coupon}}',
            '{{%beli_event_detail}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_detail-created_by}}',
            '{{%beli_event_detail}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-beli_event_detail-created_by}}',
            '{{%beli_event_detail}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_detail-updated_by}}',
            '{{%beli_event_detail}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-beli_event_detail-updated_by}}',
            '{{%beli_event_detail}}'
        );

        $this->dropTable('{{%beli_event_detail}}');
    }
}
