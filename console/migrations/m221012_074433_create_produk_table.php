<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%produk}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%kategori}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_074433_create_produk_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%produk}}', [
            'id' => $this->primaryKey(),
            'kode_sku' => $this->string()->notNull()->unique(),
            'id_kategori' => $this->integer()->notNull(),
            'nama' => $this->string()->notNull(),
            'harga' => $this->bigInteger()->notNull(),
            'stok' => $this->integer(),
            'stok_unlimited' => $this->boolean()->defaultValue(0),
            'pre_order' => $this->boolean()->defaultValue(0),
            'foto' => $this->string(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_kategori`
        $this->createIndex(
            '{{%idx-produk-id_kategori}}',
            '{{%produk}}',
            'id_kategori'
        );

        // add foreign key for table `{{%kategori}}`
        $this->addForeignKey(
            '{{%fk-produk-id_kategori}}',
            '{{%produk}}',
            'id_kategori',
            '{{%kategori}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-produk-created_by}}',
            '{{%produk}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-produk-created_by}}',
            '{{%produk}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-produk-updated_by}}',
            '{{%produk}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-produk-updated_by}}',
            '{{%produk}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%kategori}}`
        $this->dropForeignKey(
            '{{%fk-produk-id_kategori}}',
            '{{%produk}}'
        );

        // drops index for column `id_kategori`
        $this->dropIndex(
            '{{%idx-produk-id_kategori}}',
            '{{%produk}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-produk-created_by}}',
            '{{%produk}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-produk-created_by}}',
            '{{%produk}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-produk-updated_by}}',
            '{{%produk}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-produk-updated_by}}',
            '{{%produk}}'
        );

        $this->dropTable('{{%produk}}');
    }
}
