<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%beli_produk_history_status}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%beli_produk}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_082819_create_beli_produk_history_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%beli_produk_history_status}}', [
            'id' => $this->primaryKey(),
            'id_beli_produk' => $this->integer()->notNull(),
            'status_beli' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_beli_produk`
        $this->createIndex(
            '{{%idx-beli_produk_history_status-id_beli_produk}}',
            '{{%beli_produk_history_status}}',
            'id_beli_produk'
        );

        // add foreign key for table `{{%beli_produk}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_history_status-id_beli_produk}}',
            '{{%beli_produk_history_status}}',
            'id_beli_produk',
            '{{%beli_produk}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-beli_produk_history_status-created_by}}',
            '{{%beli_produk_history_status}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_history_status-created_by}}',
            '{{%beli_produk_history_status}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-beli_produk_history_status-updated_by}}',
            '{{%beli_produk_history_status}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_history_status-updated_by}}',
            '{{%beli_produk_history_status}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%beli_produk}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_history_status-id_beli_produk}}',
            '{{%beli_produk_history_status}}'
        );

        // drops index for column `id_beli_produk`
        $this->dropIndex(
            '{{%idx-beli_produk_history_status-id_beli_produk}}',
            '{{%beli_produk_history_status}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_history_status-created_by}}',
            '{{%beli_produk_history_status}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-beli_produk_history_status-created_by}}',
            '{{%beli_produk_history_status}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_history_status-updated_by}}',
            '{{%beli_produk_history_status}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-beli_produk_history_status-updated_by}}',
            '{{%beli_produk_history_status}}'
        );

        $this->dropTable('{{%beli_produk_history_status}}');
    }
}
