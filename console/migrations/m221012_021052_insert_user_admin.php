<?php

use yii\db\Migration;

/**
 * Class m221012_021052_insert_user_admin
 */
class m221012_021052_insert_user_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'username' => 'admin',
            'email' => 'khanif.zyen@gmail.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('persiapanreuniakbar2022'),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221012_021052_insert_user_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221012_021052_insert_user_admin cannot be reverted.\n";

        return false;
    }
    */
}
