<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%beli_event_history_status}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%beli_event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_072858_create_beli_event_history_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%beli_event_history_status}}', [
            'id' => $this->primaryKey(),
            'id_beli_event' => $this->integer(),
            'status_bayar' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_beli_event`
        $this->createIndex(
            '{{%idx-beli_event_history_status-id_beli_event}}',
            '{{%beli_event_history_status}}',
            'id_beli_event'
        );

        // add foreign key for table `{{%beli_event}}`
        $this->addForeignKey(
            '{{%fk-beli_event_history_status-id_beli_event}}',
            '{{%beli_event_history_status}}',
            'id_beli_event',
            '{{%beli_event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-beli_event_history_status-created_by}}',
            '{{%beli_event_history_status}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_event_history_status-created_by}}',
            '{{%beli_event_history_status}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-beli_event_history_status-updated_by}}',
            '{{%beli_event_history_status}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_event_history_status-updated_by}}',
            '{{%beli_event_history_status}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%beli_event}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_history_status-id_beli_event}}',
            '{{%beli_event_history_status}}'
        );

        // drops index for column `id_beli_event`
        $this->dropIndex(
            '{{%idx-beli_event_history_status-id_beli_event}}',
            '{{%beli_event_history_status}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_history_status-created_by}}',
            '{{%beli_event_history_status}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-beli_event_history_status-created_by}}',
            '{{%beli_event_history_status}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_event_history_status-updated_by}}',
            '{{%beli_event_history_status}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-beli_event_history_status-updated_by}}',
            '{{%beli_event_history_status}}'
        );

        $this->dropTable('{{%beli_event_history_status}}');
    }
}
