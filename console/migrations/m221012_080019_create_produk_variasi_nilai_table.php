<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%produk_variasi_nilai}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%produk}}`
 * - `{{%variasi_nilai}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_080019_create_produk_variasi_nilai_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%produk_variasi_nilai}}', [
            'id' => $this->primaryKey(),
            'kode_sku' => $this->string()->notNull()->unique(),
            'id_produk' => $this->integer()->notNull(),
            'id_variasi_nilai' => $this->integer()->notNull(),
            'harga' => $this->bigInteger()->notNull(),
            'stok' => $this->integer()->notNull(),
            'stok_unlimited' => $this->boolean()->notNull()->defaultValue(0),
            'pre_order' => $this->boolean()->defaultValue(0),
            'foto' => $this->string(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_produk`
        $this->createIndex(
            '{{%idx-produk_variasi_nilai-id_produk}}',
            '{{%produk_variasi_nilai}}',
            'id_produk'
        );

        // add foreign key for table `{{%produk}}`
        $this->addForeignKey(
            '{{%fk-produk_variasi_nilai-id_produk}}',
            '{{%produk_variasi_nilai}}',
            'id_produk',
            '{{%produk}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_variasi_nilai`
        $this->createIndex(
            '{{%idx-produk_variasi_nilai-id_variasi_nilai}}',
            '{{%produk_variasi_nilai}}',
            'id_variasi_nilai'
        );

        // add foreign key for table `{{%variasi_nilai}}`
        $this->addForeignKey(
            '{{%fk-produk_variasi_nilai-id_variasi_nilai}}',
            '{{%produk_variasi_nilai}}',
            'id_variasi_nilai',
            '{{%variasi_nilai}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-produk_variasi_nilai-created_by}}',
            '{{%produk_variasi_nilai}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-produk_variasi_nilai-created_by}}',
            '{{%produk_variasi_nilai}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-produk_variasi_nilai-updated_by}}',
            '{{%produk_variasi_nilai}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-produk_variasi_nilai-updated_by}}',
            '{{%produk_variasi_nilai}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%produk}}`
        $this->dropForeignKey(
            '{{%fk-produk_variasi_nilai-id_produk}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops index for column `id_produk`
        $this->dropIndex(
            '{{%idx-produk_variasi_nilai-id_produk}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops foreign key for table `{{%variasi_nilai}}`
        $this->dropForeignKey(
            '{{%fk-produk_variasi_nilai-id_variasi_nilai}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops index for column `id_variasi_nilai`
        $this->dropIndex(
            '{{%idx-produk_variasi_nilai-id_variasi_nilai}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-produk_variasi_nilai-created_by}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-produk_variasi_nilai-created_by}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-produk_variasi_nilai-updated_by}}',
            '{{%produk_variasi_nilai}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-produk_variasi_nilai-updated_by}}',
            '{{%produk_variasi_nilai}}'
        );

        $this->dropTable('{{%produk_variasi_nilai}}');
    }
}
