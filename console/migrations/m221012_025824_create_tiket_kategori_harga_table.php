<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tiket_kategori_harga}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_025824_create_tiket_kategori_harga_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tiket_kategori_harga}}', [
            'id' => $this->primaryKey(),
            'id_event' => $this->integer()->notNull(),
            'nama' => $this->string()->notNull(),
            'harga' => $this->integer()->notNull(),
            'total_tiket' => $this->integer(),
            'tiket_tersisa' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_event`
        $this->createIndex(
            '{{%idx-tiket_kategori_harga-id_event}}',
            '{{%tiket_kategori_harga}}',
            'id_event'
        );

        // add foreign key for table `{{%event}}`
        $this->addForeignKey(
            '{{%fk-tiket_kategori_harga-id_event}}',
            '{{%tiket_kategori_harga}}',
            'id_event',
            '{{%event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-tiket_kategori_harga-created_by}}',
            '{{%tiket_kategori_harga}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-tiket_kategori_harga-created_by}}',
            '{{%tiket_kategori_harga}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-tiket_kategori_harga-updated_by}}',
            '{{%tiket_kategori_harga}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-tiket_kategori_harga-updated_by}}',
            '{{%tiket_kategori_harga}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%event}}`
        $this->dropForeignKey(
            '{{%fk-tiket_kategori_harga-id_event}}',
            '{{%tiket_kategori_harga}}'
        );

        // drops index for column `id_event`
        $this->dropIndex(
            '{{%idx-tiket_kategori_harga-id_event}}',
            '{{%tiket_kategori_harga}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-tiket_kategori_harga-created_by}}',
            '{{%tiket_kategori_harga}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-tiket_kategori_harga-created_by}}',
            '{{%tiket_kategori_harga}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-tiket_kategori_harga-updated_by}}',
            '{{%tiket_kategori_harga}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-tiket_kategori_harga-updated_by}}',
            '{{%tiket_kategori_harga}}'
        );

        $this->dropTable('{{%tiket_kategori_harga}}');
    }
}
