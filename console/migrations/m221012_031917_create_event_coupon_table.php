<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%event_coupon}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_031917_create_event_coupon_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%event_coupon}}', [
            'id' => $this->primaryKey(),
            'id_event' => $this->integer()->notNull(),
            'keyword' => $this->string()->notNull(),
            'diskon' => $this->integer(),
            'tanggal_mulai' => $this->dateTime(),
            'tanggal_selesai' => $this->dateTime(),
            'jumlah_maksimal' => $this->integer(),
            'sisa' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_event`
        $this->createIndex(
            '{{%idx-event_coupon-id_event}}',
            '{{%event_coupon}}',
            'id_event'
        );

        // add foreign key for table `{{%event}}`
        $this->addForeignKey(
            '{{%fk-event_coupon-id_event}}',
            '{{%event_coupon}}',
            'id_event',
            '{{%event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-event_coupon-created_by}}',
            '{{%event_coupon}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_coupon-created_by}}',
            '{{%event_coupon}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-event_coupon-updated_by}}',
            '{{%event_coupon}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_coupon-updated_by}}',
            '{{%event_coupon}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%event}}`
        $this->dropForeignKey(
            '{{%fk-event_coupon-id_event}}',
            '{{%event_coupon}}'
        );

        // drops index for column `id_event`
        $this->dropIndex(
            '{{%idx-event_coupon-id_event}}',
            '{{%event_coupon}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_coupon-created_by}}',
            '{{%event_coupon}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-event_coupon-created_by}}',
            '{{%event_coupon}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_coupon-updated_by}}',
            '{{%event_coupon}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-event_coupon-updated_by}}',
            '{{%event_coupon}}'
        );

        $this->dropTable('{{%event_coupon}}');
    }
}
