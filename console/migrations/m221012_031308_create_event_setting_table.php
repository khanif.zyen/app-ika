<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%event_setting}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_031308_create_event_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%event_setting}}', [
            'id' => $this->primaryKey(),
            'id_event' => $this->integer()->notNull(),
            'tampilkan_tiket_tersisa' => $this->boolean()->defaultValue(0),
            'tampilkan_tiket_tersisa_kategori' => $this->boolean()->defaultValue(0),
            'tampilkan_tiket_terjual' => $this->boolean()->defaultValue(1),
            'maksimal_pembelian_tiket' => $this->integer()->notNull()->defaultValue(10),
            'peta_lokasi_long' => $this->decimal(),
            'peta_lokasi_lat' => $this->decimal(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_event`
        $this->createIndex(
            '{{%idx-event_setting-id_event}}',
            '{{%event_setting}}',
            'id_event'
        );

        // add foreign key for table `{{%event}}`
        $this->addForeignKey(
            '{{%fk-event_setting-id_event}}',
            '{{%event_setting}}',
            'id_event',
            '{{%event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-event_setting-created_by}}',
            '{{%event_setting}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_setting-created_by}}',
            '{{%event_setting}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-event_setting-updated_by}}',
            '{{%event_setting}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-event_setting-updated_by}}',
            '{{%event_setting}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%event}}`
        $this->dropForeignKey(
            '{{%fk-event_setting-id_event}}',
            '{{%event_setting}}'
        );

        // drops index for column `id_event`
        $this->dropIndex(
            '{{%idx-event_setting-id_event}}',
            '{{%event_setting}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_setting-created_by}}',
            '{{%event_setting}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-event_setting-created_by}}',
            '{{%event_setting}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-event_setting-updated_by}}',
            '{{%event_setting}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-event_setting-updated_by}}',
            '{{%event_setting}}'
        );

        $this->dropTable('{{%event_setting}}');
    }
}
