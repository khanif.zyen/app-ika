<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%beli_event}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_033510_create_beli_event_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%beli_event}}', [
            'id' => $this->primaryKey(),
            'id_event' => $this->integer()->notNull(),
            'kode_beli' => $this->string()->notNull(),
            'qty_tiket' => $this->integer()->notNull(),
            'total_bayar' => $this->bigInteger()->notNull(),
            'status_bayar' => $this->integer()->defaultValue(9),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_event`
        $this->createIndex(
            '{{%idx-beli_event-id_event}}',
            '{{%beli_event}}',
            'id_event'
        );

        // add foreign key for table `{{%event}}`
        $this->addForeignKey(
            '{{%fk-beli_event-id_event}}',
            '{{%beli_event}}',
            'id_event',
            '{{%event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-beli_event-created_by}}',
            '{{%beli_event}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_event-created_by}}',
            '{{%beli_event}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-beli_event-updated_by}}',
            '{{%beli_event}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_event-updated_by}}',
            '{{%beli_event}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%event}}`
        $this->dropForeignKey(
            '{{%fk-beli_event-id_event}}',
            '{{%beli_event}}'
        );

        // drops index for column `id_event`
        $this->dropIndex(
            '{{%idx-beli_event-id_event}}',
            '{{%beli_event}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_event-created_by}}',
            '{{%beli_event}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-beli_event-created_by}}',
            '{{%beli_event}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_event-updated_by}}',
            '{{%beli_event}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-beli_event-updated_by}}',
            '{{%beli_event}}'
        );

        $this->dropTable('{{%beli_event}}');
    }
}
