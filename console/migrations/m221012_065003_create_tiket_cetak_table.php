<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tiket_cetak}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%event}}`
 * - `{{%beli_event}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_065003_create_tiket_cetak_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tiket_cetak}}', [
            'id' => $this->primaryKey(),
            'id_event' => $this->integer()->notNull(),
            'id_beli_event' => $this->integer(),
            'kode_tiket' => $this->string()->notNull()->unique(),
            'keterangan' => $this->string(),
            'sudah_digunakan' => $this->boolean()->notNull()->defaultValue(0),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_event`
        $this->createIndex(
            '{{%idx-tiket_cetak-id_event}}',
            '{{%tiket_cetak}}',
            'id_event'
        );

        // add foreign key for table `{{%event}}`
        $this->addForeignKey(
            '{{%fk-tiket_cetak-id_event}}',
            '{{%tiket_cetak}}',
            'id_event',
            '{{%event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_beli_event`
        $this->createIndex(
            '{{%idx-tiket_cetak-id_beli_event}}',
            '{{%tiket_cetak}}',
            'id_beli_event'
        );

        // add foreign key for table `{{%beli_event}}`
        $this->addForeignKey(
            '{{%fk-tiket_cetak-id_beli_event}}',
            '{{%tiket_cetak}}',
            'id_beli_event',
            '{{%beli_event}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-tiket_cetak-created_by}}',
            '{{%tiket_cetak}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-tiket_cetak-created_by}}',
            '{{%tiket_cetak}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-tiket_cetak-updated_by}}',
            '{{%tiket_cetak}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-tiket_cetak-updated_by}}',
            '{{%tiket_cetak}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%event}}`
        $this->dropForeignKey(
            '{{%fk-tiket_cetak-id_event}}',
            '{{%tiket_cetak}}'
        );

        // drops index for column `id_event`
        $this->dropIndex(
            '{{%idx-tiket_cetak-id_event}}',
            '{{%tiket_cetak}}'
        );

        // drops foreign key for table `{{%beli_event}}`
        $this->dropForeignKey(
            '{{%fk-tiket_cetak-id_beli_event}}',
            '{{%tiket_cetak}}'
        );

        // drops index for column `id_beli_event`
        $this->dropIndex(
            '{{%idx-tiket_cetak-id_beli_event}}',
            '{{%tiket_cetak}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-tiket_cetak-created_by}}',
            '{{%tiket_cetak}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-tiket_cetak-created_by}}',
            '{{%tiket_cetak}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-tiket_cetak-updated_by}}',
            '{{%tiket_cetak}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-tiket_cetak-updated_by}}',
            '{{%tiket_cetak}}'
        );

        $this->dropTable('{{%tiket_cetak}}');
    }
}
