<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%kategori}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_073613_create_kategori_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%kategori}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-kategori-created_by}}',
            '{{%kategori}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-kategori-created_by}}',
            '{{%kategori}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-kategori-updated_by}}',
            '{{%kategori}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-kategori-updated_by}}',
            '{{%kategori}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-kategori-created_by}}',
            '{{%kategori}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-kategori-created_by}}',
            '{{%kategori}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-kategori-updated_by}}',
            '{{%kategori}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-kategori-updated_by}}',
            '{{%kategori}}'
        );

        $this->dropTable('{{%kategori}}');
    }
}
