<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%beli_produk_detail}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%beli_produk}}`
 * - `{{%produk_variasi_nilai}}`
 * - `{{%produk}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_082139_create_beli_produk_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%beli_produk_detail}}', [
            'id' => $this->primaryKey(),
            'id_beli_produk' => $this->integer()->notNull(),
            'id_produk_variasi_nilai' => $this->integer(),
            'id_produk' => $this->integer(),
            'harga' => $this->bigInteger()->notNull(),
            'jumlah' => $this->integer()->notNull(),
            'total_harga' => $this->bigInteger()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_beli_produk`
        $this->createIndex(
            '{{%idx-beli_produk_detail-id_beli_produk}}',
            '{{%beli_produk_detail}}',
            'id_beli_produk'
        );

        // add foreign key for table `{{%beli_produk}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_detail-id_beli_produk}}',
            '{{%beli_produk_detail}}',
            'id_beli_produk',
            '{{%beli_produk}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_produk_variasi_nilai`
        $this->createIndex(
            '{{%idx-beli_produk_detail-id_produk_variasi_nilai}}',
            '{{%beli_produk_detail}}',
            'id_produk_variasi_nilai'
        );

        // add foreign key for table `{{%produk_variasi_nilai}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_detail-id_produk_variasi_nilai}}',
            '{{%beli_produk_detail}}',
            'id_produk_variasi_nilai',
            '{{%produk_variasi_nilai}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_produk`
        $this->createIndex(
            '{{%idx-beli_produk_detail-id_produk}}',
            '{{%beli_produk_detail}}',
            'id_produk'
        );

        // add foreign key for table `{{%produk}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_detail-id_produk}}',
            '{{%beli_produk_detail}}',
            'id_produk',
            '{{%produk}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-beli_produk_detail-created_by}}',
            '{{%beli_produk_detail}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_detail-created_by}}',
            '{{%beli_produk_detail}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-beli_produk_detail-updated_by}}',
            '{{%beli_produk_detail}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_produk_detail-updated_by}}',
            '{{%beli_produk_detail}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%beli_produk}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_detail-id_beli_produk}}',
            '{{%beli_produk_detail}}'
        );

        // drops index for column `id_beli_produk`
        $this->dropIndex(
            '{{%idx-beli_produk_detail-id_beli_produk}}',
            '{{%beli_produk_detail}}'
        );

        // drops foreign key for table `{{%produk_variasi_nilai}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_detail-id_produk_variasi_nilai}}',
            '{{%beli_produk_detail}}'
        );

        // drops index for column `id_produk_variasi_nilai`
        $this->dropIndex(
            '{{%idx-beli_produk_detail-id_produk_variasi_nilai}}',
            '{{%beli_produk_detail}}'
        );

        // drops foreign key for table `{{%produk}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_detail-id_produk}}',
            '{{%beli_produk_detail}}'
        );

        // drops index for column `id_produk`
        $this->dropIndex(
            '{{%idx-beli_produk_detail-id_produk}}',
            '{{%beli_produk_detail}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_detail-created_by}}',
            '{{%beli_produk_detail}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-beli_produk_detail-created_by}}',
            '{{%beli_produk_detail}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk_detail-updated_by}}',
            '{{%beli_produk_detail}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-beli_produk_detail-updated_by}}',
            '{{%beli_produk_detail}}'
        );

        $this->dropTable('{{%beli_produk_detail}}');
    }
}
