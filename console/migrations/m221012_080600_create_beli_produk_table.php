<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%beli_produk}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_080600_create_beli_produk_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%beli_produk}}', [
            'id' => $this->primaryKey(),
            'kode_beli' => $this->string()->notNull()->unique(),
            'tanggal_beli' => $this->dateTime()->notNull(),
            'qty_produk' => $this->integer()->notNull(),
            'total_bayar' => $this->bigInteger()->notNull(),
            'status_bayar' => $this->integer()->notNull(),
            'status_beli' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-beli_produk-created_by}}',
            '{{%beli_produk}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_produk-created_by}}',
            '{{%beli_produk}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-beli_produk-updated_by}}',
            '{{%beli_produk}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-beli_produk-updated_by}}',
            '{{%beli_produk}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk-created_by}}',
            '{{%beli_produk}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-beli_produk-created_by}}',
            '{{%beli_produk}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-beli_produk-updated_by}}',
            '{{%beli_produk}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-beli_produk-updated_by}}',
            '{{%beli_produk}}'
        );

        $this->dropTable('{{%beli_produk}}');
    }
}
