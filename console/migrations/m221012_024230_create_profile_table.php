<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profile}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%pekerjaan}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m221012_024230_create_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'nama' => $this->string()->notNull(),
            'alamat' => $this->string()->notNull(),
            'no_hp' => $this->string()->notNull(),
            'foto' => $this->string(),
            'id_pekerjaan' => $this->integer(),
            'pekerjaan_lainnya' => $this->string(),
            'lulus_tahun' => $this->integer()->notNull(),
            'status_anggota' => $this->integer()->defaultValue(9),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        // creates index for column `id_user`
        $this->createIndex(
            '{{%idx-profile-id_user}}',
            '{{%profile}}',
            'id_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-profile-id_user}}',
            '{{%profile}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_pekerjaan`
        $this->createIndex(
            '{{%idx-profile-id_pekerjaan}}',
            '{{%profile}}',
            'id_pekerjaan'
        );

        // add foreign key for table `{{%pekerjaan}}`
        $this->addForeignKey(
            '{{%fk-profile-id_pekerjaan}}',
            '{{%profile}}',
            'id_pekerjaan',
            '{{%pekerjaan}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-profile-created_by}}',
            '{{%profile}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-profile-created_by}}',
            '{{%profile}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-profile-updated_by}}',
            '{{%profile}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-profile-updated_by}}',
            '{{%profile}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-profile-id_user}}',
            '{{%profile}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            '{{%idx-profile-id_user}}',
            '{{%profile}}'
        );

        // drops foreign key for table `{{%pekerjaan}}`
        $this->dropForeignKey(
            '{{%fk-profile-id_pekerjaan}}',
            '{{%profile}}'
        );

        // drops index for column `id_pekerjaan`
        $this->dropIndex(
            '{{%idx-profile-id_pekerjaan}}',
            '{{%profile}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-profile-created_by}}',
            '{{%profile}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-profile-created_by}}',
            '{{%profile}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-profile-updated_by}}',
            '{{%profile}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-profile-updated_by}}',
            '{{%profile}}'
        );

        $this->dropTable('{{%profile}}');
    }
}
