<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property int $id
 * @property int $owner
 * @property string $nama
 * @property string|null $deskripsi
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property int|null $event_seharian
 * @property string $tempat
 * @property string|null $alamat
 * @property string|null $foto
 * @property int|null $total_tiket
 * @property int|null $tiket_unlimited
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliEvent[] $beliEvents
 * @property User $createdBy
 * @property EventCoupon[] $eventCoupons
 * @property EventPanitium[] $eventPanitia
 * @property EventSetting[] $eventSettings
 * @property User $owner0
 * @property TiketCetak[] $tiketCetaks
 * @property TiketKategoriHarga[] $tiketKategoriHargas
 * @property User $updatedBy
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner', 'nama', 'tanggal_mulai', 'tanggal_selesai', 'tempat'], 'required'],
            [['owner', 'event_seharian', 'total_tiket', 'tiket_unlimited', 'created_by', 'updated_by'], 'integer'],
            [['deskripsi'], 'string'],
            [['tanggal_mulai', 'tanggal_selesai', 'created_at', 'updated_at'], 'safe'],
            [['nama', 'tempat', 'alamat', 'foto'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['owner'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['owner' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner' => Yii::t('app', 'Owner'),
            'nama' => Yii::t('app', 'Nama'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
            'tanggal_mulai' => Yii::t('app', 'Tanggal Mulai'),
            'tanggal_selesai' => Yii::t('app', 'Tanggal Selesai'),
            'event_seharian' => Yii::t('app', 'Event Seharian'),
            'tempat' => Yii::t('app', 'Tempat'),
            'alamat' => Yii::t('app', 'Alamat'),
            'foto' => Yii::t('app', 'Foto'),
            'total_tiket' => Yii::t('app', 'Total Tiket'),
            'tiket_unlimited' => Yii::t('app', 'Tiket Unlimited'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliEvents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEvents()
    {
        return $this->hasMany(BeliEvent::class, ['id_event' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[EventCoupons]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventCoupons()
    {
        return $this->hasMany(EventCoupon::class, ['id_event' => 'id']);
    }

    /**
     * Gets query for [[EventPanitia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventPanitia()
    {
        return $this->hasMany(EventPanitium::class, ['id_event' => 'id']);
    }

    /**
     * Gets query for [[EventSettings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventSettings()
    {
        return $this->hasMany(EventSetting::class, ['id_event' => 'id']);
    }

    /**
     * Gets query for [[Owner0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(User::class, ['id' => 'owner']);
    }

    /**
     * Gets query for [[TiketCetaks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiketCetaks()
    {
        return $this->hasMany(TiketCetak::class, ['id_event' => 'id']);
    }

    /**
     * Gets query for [[TiketKategoriHargas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiketKategoriHargas()
    {
        return $this->hasMany(TiketKategoriHarga::class, ['id_event' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
