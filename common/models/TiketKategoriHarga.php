<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tiket_kategori_harga}}".
 *
 * @property int $id
 * @property int $id_event
 * @property string $nama
 * @property int $harga
 * @property int|null $total_tiket
 * @property int|null $tiket_tersisa
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliEventDetail[] $beliEventDetails
 * @property User $createdBy
 * @property Event $event
 * @property User $updatedBy
 */
class TiketKategoriHarga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tiket_kategori_harga}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_event', 'nama', 'harga'], 'required'],
            [['id_event', 'harga', 'total_tiket', 'tiket_tersisa', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_event'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['id_event' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_event' => Yii::t('app', 'Id Event'),
            'nama' => Yii::t('app', 'Nama'),
            'harga' => Yii::t('app', 'Harga'),
            'total_tiket' => Yii::t('app', 'Total Tiket'),
            'tiket_tersisa' => Yii::t('app', 'Tiket Tersisa'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliEventDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEventDetails()
    {
        return $this->hasMany(BeliEventDetail::class, ['id_tiket_kategori_harga' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'id_event']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
