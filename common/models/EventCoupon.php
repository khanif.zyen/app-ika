<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%event_coupon}}".
 *
 * @property int $id
 * @property int $id_event
 * @property string $keyword
 * @property int|null $diskon
 * @property string|null $tanggal_mulai
 * @property string|null $tanggal_selesai
 * @property int|null $jumlah_maksimal
 * @property int|null $sisa
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliEventDetail[] $beliEventDetails
 * @property User $createdBy
 * @property Event $event
 * @property User $updatedBy
 */
class EventCoupon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%event_coupon}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_event', 'keyword'], 'required'],
            [['id_event', 'diskon', 'jumlah_maksimal', 'sisa', 'created_by', 'updated_by'], 'integer'],
            [['tanggal_mulai', 'tanggal_selesai', 'created_at', 'updated_at'], 'safe'],
            [['keyword'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_event'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['id_event' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_event' => Yii::t('app', 'Id Event'),
            'keyword' => Yii::t('app', 'Keyword'),
            'diskon' => Yii::t('app', 'Diskon'),
            'tanggal_mulai' => Yii::t('app', 'Tanggal Mulai'),
            'tanggal_selesai' => Yii::t('app', 'Tanggal Selesai'),
            'jumlah_maksimal' => Yii::t('app', 'Jumlah Maksimal'),
            'sisa' => Yii::t('app', 'Sisa'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliEventDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEventDetails()
    {
        return $this->hasMany(BeliEventDetail::class, ['id_event_coupon' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'id_event']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
