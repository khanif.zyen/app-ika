<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%beli_produk_history_status}}".
 *
 * @property int $id
 * @property int $id_beli_produk
 * @property int|null $status_beli
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliProduk $beliProduk
 * @property User $createdBy
 * @property User $updatedBy
 */
class BeliProdukHistoryStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%beli_produk_history_status}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_beli_produk'], 'required'],
            [['id_beli_produk', 'status_beli', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_beli_produk'], 'exist', 'skipOnError' => true, 'targetClass' => BeliProduk::class, 'targetAttribute' => ['id_beli_produk' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_beli_produk' => Yii::t('app', 'Id Beli Produk'),
            'status_beli' => Yii::t('app', 'Status Beli'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliProduk]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliProduk()
    {
        return $this->hasOne(BeliProduk::class, ['id' => 'id_beli_produk']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
