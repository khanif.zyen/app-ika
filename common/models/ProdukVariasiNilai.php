<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%produk_variasi_nilai}}".
 *
 * @property int $id
 * @property string $kode_sku
 * @property int $id_produk
 * @property int $id_variasi_nilai
 * @property int $harga
 * @property int $stok
 * @property int $stok_unlimited
 * @property int|null $pre_order
 * @property string|null $foto
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliProdukDetail[] $beliProdukDetails
 * @property User $createdBy
 * @property Produk $produk
 * @property User $updatedBy
 * @property VariasiNilai $variasiNilai
 */
class ProdukVariasiNilai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%produk_variasi_nilai}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_sku', 'id_produk', 'id_variasi_nilai', 'harga', 'stok'], 'required'],
            [['id_produk', 'id_variasi_nilai', 'harga', 'stok', 'stok_unlimited', 'pre_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_sku', 'foto'], 'string', 'max' => 255],
            [['kode_sku'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_produk'], 'exist', 'skipOnError' => true, 'targetClass' => Produk::class, 'targetAttribute' => ['id_produk' => 'id']],
            [['id_variasi_nilai'], 'exist', 'skipOnError' => true, 'targetClass' => VariasiNilai::class, 'targetAttribute' => ['id_variasi_nilai' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode_sku' => Yii::t('app', 'Kode Sku'),
            'id_produk' => Yii::t('app', 'Id Produk'),
            'id_variasi_nilai' => Yii::t('app', 'Id Variasi Nilai'),
            'harga' => Yii::t('app', 'Harga'),
            'stok' => Yii::t('app', 'Stok'),
            'stok_unlimited' => Yii::t('app', 'Stok Unlimited'),
            'pre_order' => Yii::t('app', 'Pre Order'),
            'foto' => Yii::t('app', 'Foto'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliProdukDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliProdukDetails()
    {
        return $this->hasMany(BeliProdukDetail::class, ['id_produk_variasi_nilai' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Produk]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduk()
    {
        return $this->hasOne(Produk::class, ['id' => 'id_produk']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[VariasiNilai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVariasiNilai()
    {
        return $this->hasOne(VariasiNilai::class, ['id' => 'id_variasi_nilai']);
    }
}
