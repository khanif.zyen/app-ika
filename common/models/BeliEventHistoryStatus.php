<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%beli_event_history_status}}".
 *
 * @property int $id
 * @property int|null $id_beli_event
 * @property int|null $status_bayar
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliEvent $beliEvent
 * @property User $createdBy
 * @property User $updatedBy
 */
class BeliEventHistoryStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%beli_event_history_status}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_beli_event', 'status_bayar', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_beli_event'], 'exist', 'skipOnError' => true, 'targetClass' => BeliEvent::class, 'targetAttribute' => ['id_beli_event' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_beli_event' => Yii::t('app', 'Id Beli Event'),
            'status_bayar' => Yii::t('app', 'Status Bayar'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliEvent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEvent()
    {
        return $this->hasOne(BeliEvent::class, ['id' => 'id_beli_event']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
