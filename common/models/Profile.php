<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property int $id
 * @property int|null $id_user
 * @property string $nama
 * @property string $alamat
 * @property string $no_hp
 * @property string|null $foto
 * @property int|null $id_pekerjaan
 * @property string|null $pekerjaan_lainnya
 * @property int $lulus_tahun
 * @property int|null $status_anggota
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $createdBy
 * @property Pekerjaan $pekerjaan
 * @property User $updatedBy
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_pekerjaan', 'lulus_tahun', 'status_anggota', 'created_by', 'updated_by'], 'integer'],
            [['nama', 'alamat', 'no_hp', 'lulus_tahun'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama', 'alamat', 'no_hp', 'foto', 'pekerjaan_lainnya'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_pekerjaan'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::class, 'targetAttribute' => ['id_pekerjaan' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_user' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'nama' => Yii::t('app', 'Nama'),
            'alamat' => Yii::t('app', 'Alamat'),
            'no_hp' => Yii::t('app', 'No Hp'),
            'foto' => Yii::t('app', 'Foto'),
            'id_pekerjaan' => Yii::t('app', 'Id Pekerjaan'),
            'pekerjaan_lainnya' => Yii::t('app', 'Pekerjaan Lainnya'),
            'lulus_tahun' => Yii::t('app', 'Lulus Tahun'),
            'status_anggota' => Yii::t('app', 'Status Anggota'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Pekerjaan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaan()
    {
        return $this->hasOne(Pekerjaan::class, ['id' => 'id_pekerjaan']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }
}
