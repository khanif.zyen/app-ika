<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%beli_event}}".
 *
 * @property int $id
 * @property int $id_event
 * @property string $kode_beli
 * @property int $qty_tiket
 * @property int $total_bayar
 * @property int|null $status_bayar
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliEventDetail[] $beliEventDetails
 * @property BeliEventHistoryStatus[] $beliEventHistoryStatuses
 * @property User $createdBy
 * @property Event $event
 * @property TiketCetak[] $tiketCetaks
 * @property User $updatedBy
 */
class BeliEvent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%beli_event}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_event', 'kode_beli', 'qty_tiket', 'total_bayar'], 'required'],
            [['id_event', 'qty_tiket', 'total_bayar', 'status_bayar', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_beli'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_event'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['id_event' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_event' => Yii::t('app', 'Id Event'),
            'kode_beli' => Yii::t('app', 'Kode Beli'),
            'qty_tiket' => Yii::t('app', 'Qty Tiket'),
            'total_bayar' => Yii::t('app', 'Total Bayar'),
            'status_bayar' => Yii::t('app', 'Status Bayar'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliEventDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEventDetails()
    {
        return $this->hasMany(BeliEventDetail::class, ['id_beli_event' => 'id']);
    }

    /**
     * Gets query for [[BeliEventHistoryStatuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEventHistoryStatuses()
    {
        return $this->hasMany(BeliEventHistoryStatus::class, ['id_beli_event' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'id_event']);
    }

    /**
     * Gets query for [[TiketCetaks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiketCetaks()
    {
        return $this->hasMany(TiketCetak::class, ['id_beli_event' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
