<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%variasi_nilai}}".
 *
 * @property int $id
 * @property int $id_variasi
 * @property string $nilai
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $createdBy
 * @property ProdukVariasiNilai[] $produkVariasiNilais
 * @property User $updatedBy
 * @property Variasi $variasi
 */
class VariasiNilai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%variasi_nilai}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_variasi', 'nilai'], 'required'],
            [['id_variasi', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nilai'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_variasi'], 'exist', 'skipOnError' => true, 'targetClass' => Variasi::class, 'targetAttribute' => ['id_variasi' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_variasi' => Yii::t('app', 'Id Variasi'),
            'nilai' => Yii::t('app', 'Nilai'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[ProdukVariasiNilais]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdukVariasiNilais()
    {
        return $this->hasMany(ProdukVariasiNilai::class, ['id_variasi_nilai' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[Variasi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVariasi()
    {
        return $this->hasOne(Variasi::class, ['id' => 'id_variasi']);
    }
}
