<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%event_setting}}".
 *
 * @property int $id
 * @property int $id_event
 * @property int|null $tampilkan_tiket_tersisa
 * @property int|null $tampilkan_tiket_tersisa_kategori
 * @property int|null $tampilkan_tiket_terjual
 * @property int $maksimal_pembelian_tiket
 * @property float|null $peta_lokasi_long
 * @property float|null $peta_lokasi_lat
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $createdBy
 * @property Event $event
 * @property User $updatedBy
 */
class EventSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%event_setting}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_event'], 'required'],
            [['id_event', 'tampilkan_tiket_tersisa', 'tampilkan_tiket_tersisa_kategori', 'tampilkan_tiket_terjual', 'maksimal_pembelian_tiket', 'created_by', 'updated_by'], 'integer'],
            [['peta_lokasi_long', 'peta_lokasi_lat'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_event'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['id_event' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_event' => Yii::t('app', 'Id Event'),
            'tampilkan_tiket_tersisa' => Yii::t('app', 'Tampilkan Tiket Tersisa'),
            'tampilkan_tiket_tersisa_kategori' => Yii::t('app', 'Tampilkan Tiket Tersisa Kategori'),
            'tampilkan_tiket_terjual' => Yii::t('app', 'Tampilkan Tiket Terjual'),
            'maksimal_pembelian_tiket' => Yii::t('app', 'Maksimal Pembelian Tiket'),
            'peta_lokasi_long' => Yii::t('app', 'Peta Lokasi Long'),
            'peta_lokasi_lat' => Yii::t('app', 'Peta Lokasi Lat'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'id_event']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
