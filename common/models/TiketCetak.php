<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tiket_cetak}}".
 *
 * @property int $id
 * @property int $id_event
 * @property int|null $id_beli_event
 * @property string $kode_tiket
 * @property string|null $keterangan
 * @property int $sudah_digunakan
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliEvent $beliEvent
 * @property User $createdBy
 * @property Event $event
 * @property User $updatedBy
 */
class TiketCetak extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tiket_cetak}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_event', 'kode_tiket'], 'required'],
            [['id_event', 'id_beli_event', 'sudah_digunakan', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_tiket', 'keterangan'], 'string', 'max' => 255],
            [['kode_tiket'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_beli_event'], 'exist', 'skipOnError' => true, 'targetClass' => BeliEvent::class, 'targetAttribute' => ['id_beli_event' => 'id']],
            [['id_event'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['id_event' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_event' => Yii::t('app', 'Id Event'),
            'id_beli_event' => Yii::t('app', 'Id Beli Event'),
            'kode_tiket' => Yii::t('app', 'Kode Tiket'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'sudah_digunakan' => Yii::t('app', 'Sudah Digunakan'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliEvent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliEvent()
    {
        return $this->hasOne(BeliEvent::class, ['id' => 'id_beli_event']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'id_event']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
