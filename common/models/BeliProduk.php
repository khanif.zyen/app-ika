<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%beli_produk}}".
 *
 * @property int $id
 * @property string $kode_beli
 * @property string $tanggal_beli
 * @property int $qty_produk
 * @property int $total_bayar
 * @property int $status_bayar
 * @property int|null $status_beli
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliProdukDetail[] $beliProdukDetails
 * @property BeliProdukHistoryStatus[] $beliProdukHistoryStatuses
 * @property User $createdBy
 * @property User $updatedBy
 */
class BeliProduk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%beli_produk}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_beli', 'tanggal_beli', 'qty_produk', 'total_bayar', 'status_bayar'], 'required'],
            [['tanggal_beli', 'created_at', 'updated_at'], 'safe'],
            [['qty_produk', 'total_bayar', 'status_bayar', 'status_beli', 'created_by', 'updated_by'], 'integer'],
            [['kode_beli'], 'string', 'max' => 255],
            [['kode_beli'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode_beli' => Yii::t('app', 'Kode Beli'),
            'tanggal_beli' => Yii::t('app', 'Tanggal Beli'),
            'qty_produk' => Yii::t('app', 'Qty Produk'),
            'total_bayar' => Yii::t('app', 'Total Bayar'),
            'status_bayar' => Yii::t('app', 'Status Bayar'),
            'status_beli' => Yii::t('app', 'Status Beli'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliProdukDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliProdukDetails()
    {
        return $this->hasMany(BeliProdukDetail::class, ['id_beli_produk' => 'id']);
    }

    /**
     * Gets query for [[BeliProdukHistoryStatuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliProdukHistoryStatuses()
    {
        return $this->hasMany(BeliProdukHistoryStatus::class, ['id_beli_produk' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
