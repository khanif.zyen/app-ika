<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%beli_produk_detail}}".
 *
 * @property int $id
 * @property int $id_beli_produk
 * @property int|null $id_produk_variasi_nilai
 * @property int|null $id_produk
 * @property int $harga
 * @property int $jumlah
 * @property int $total_harga
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliProduk $beliProduk
 * @property User $createdBy
 * @property Produk $produk
 * @property ProdukVariasiNilai $produkVariasiNilai
 * @property User $updatedBy
 */
class BeliProdukDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%beli_produk_detail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_beli_produk', 'harga', 'jumlah', 'total_harga'], 'required'],
            [['id_beli_produk', 'id_produk_variasi_nilai', 'id_produk', 'harga', 'jumlah', 'total_harga', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_beli_produk'], 'exist', 'skipOnError' => true, 'targetClass' => BeliProduk::class, 'targetAttribute' => ['id_beli_produk' => 'id']],
            [['id_produk'], 'exist', 'skipOnError' => true, 'targetClass' => Produk::class, 'targetAttribute' => ['id_produk' => 'id']],
            [['id_produk_variasi_nilai'], 'exist', 'skipOnError' => true, 'targetClass' => ProdukVariasiNilai::class, 'targetAttribute' => ['id_produk_variasi_nilai' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_beli_produk' => Yii::t('app', 'Id Beli Produk'),
            'id_produk_variasi_nilai' => Yii::t('app', 'Id Produk Variasi Nilai'),
            'id_produk' => Yii::t('app', 'Id Produk'),
            'harga' => Yii::t('app', 'Harga'),
            'jumlah' => Yii::t('app', 'Jumlah'),
            'total_harga' => Yii::t('app', 'Total Harga'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliProduk]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliProduk()
    {
        return $this->hasOne(BeliProduk::class, ['id' => 'id_beli_produk']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Produk]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduk()
    {
        return $this->hasOne(Produk::class, ['id' => 'id_produk']);
    }

    /**
     * Gets query for [[ProdukVariasiNilai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdukVariasiNilai()
    {
        return $this->hasOne(ProdukVariasiNilai::class, ['id' => 'id_produk_variasi_nilai']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
