<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%produk}}".
 *
 * @property int $id
 * @property string $kode_sku
 * @property int $id_kategori
 * @property string $nama
 * @property int $harga
 * @property int|null $stok
 * @property int|null $stok_unlimited
 * @property int|null $pre_order
 * @property string|null $foto
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BeliProdukDetail[] $beliProdukDetails
 * @property User $createdBy
 * @property Kategori $kategori
 * @property ProdukVariasiNilai[] $produkVariasiNilais
 * @property User $updatedBy
 */
class Produk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%produk}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_sku', 'id_kategori', 'nama', 'harga'], 'required'],
            [['id_kategori', 'harga', 'stok', 'stok_unlimited', 'pre_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_sku', 'nama', 'foto'], 'string', 'max' => 255],
            [['kode_sku'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['id_kategori'], 'exist', 'skipOnError' => true, 'targetClass' => Kategori::class, 'targetAttribute' => ['id_kategori' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode_sku' => Yii::t('app', 'Kode Sku'),
            'id_kategori' => Yii::t('app', 'Id Kategori'),
            'nama' => Yii::t('app', 'Nama'),
            'harga' => Yii::t('app', 'Harga'),
            'stok' => Yii::t('app', 'Stok'),
            'stok_unlimited' => Yii::t('app', 'Stok Unlimited'),
            'pre_order' => Yii::t('app', 'Pre Order'),
            'foto' => Yii::t('app', 'Foto'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BeliProdukDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeliProdukDetails()
    {
        return $this->hasMany(BeliProdukDetail::class, ['id_produk' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Kategori]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(Kategori::class, ['id' => 'id_kategori']);
    }

    /**
     * Gets query for [[ProdukVariasiNilais]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdukVariasiNilais()
    {
        return $this->hasMany(ProdukVariasiNilai::class, ['id_produk' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
