# Aplikasi Ikatan Alumni SMAN 1 Jepara

Aplikasi ini dibuat untuk persiapan reuni akbar SMAN 1 Jepara yang akan diadakan tanggal 27 November 2022. Dibuat menggunakan Yii2 Framework dan MySQL.

# Fitur

 1. Kelola User
 2. Kelola Event
 3. Kelola Product/Shop
 4. Kelola Dana sosial (v2)
 5. Laporan Dana sosial (v2)

## Alur pendaftaran member ika
 1. User mendaftar  ke website
 2. Verifikasi  dari admin
    1. User terdaftar (not verified)
    2. User terverifikasi (verified)

## Alur pendaftaran event

1.  User login
2.  User memilih event
3.  User memasukkan  jumlah  tiket
4.  User user  menerima  tagihan
    1.  User membayar  sejumlah  tagihan > paid > cetak  tiket
    2.  User belum  membayar  dibatas  waktu (sehari) >
        1.  unpaid (hingga 24 jam)
        2.  Cancelled (lebih  dari  sehari)

## Alur pembelian product/shop

1.  User belum login > hanya  bisa browse product
2.  User login > bisa  memasukkan product ke  keranjang
3.  User melakukan checkout > keluar  tagihan
    1.  Unpaid (hingga 24 jam)
    2.  Paid (setelah 24 jam)
    3.  Processed (diubah oleh admin)
    4.  Sent (dikirim/ongkir)
    5.  Finished (selesai)
    6.  Cancelled (setelah 24 jam tidak  melakukan  pembayaran)

## Alur kelola dana sosial

1.  User belum login > tidak  bisa  melihat
2.  User login > memilih dana sosial  umum
    1.  Memasukkan nominal bebas, minimal 15.000
        1.  Unpaid (hingga 24 jam)
        2.  Paid (lunas  dalam 24 jam)
        3.  Cancelled (batal  setelah 24 jam)
3.  User login > memilih daftar dana sosial yang diperuntukkan  khusus
    1.  Pilihan nominal 25.000, 50.000, 100.000
        1.  Unpaid (hingga 24 jam)
        2.  Paid (lunas  dalam 24 jam)
        3.  Cancelled (batal  setelah 24 jam)
    2.  Pilihan nominal bebas
        1.  Unpaid (hingga 24 jam)
        2.  Paid (lunas  dalam 24 jam)  
        3.  Cancelled (batal  setelah 24 jam)

# Usage
1. Clone this project to your htdocs or public_html
   ```bash
   git clone https://gitlab.com/khanif.zyen/ika-app.git ika-app
   ```

2. Init development mode by running init and choose "0" for development mode
    ```
    D:\xampp8.1\htdocs\app-ika>init
    Yii Application Initialization Tool v1.0

    Which environment do you want the application to be initialized in?

    [0] Development
    [1] Production

    Your choice [0-1, or "q" to quit] 0
    ```
3. Change db config in common\config\main.local.php, and add tablePrefix
   ```php
       'dsn' => 'mysql:host=localhost;dbname=app-ika',
       'username' => 'root',
       'password' => '',
       'charset' => 'utf8',
       'tablePrefix' => 'tbl_'
   ```
4. Run migration command
   ```
   yii migrate/up
   ```
5. Run development server
   ```
   yii serve --docroot="frontend/web"
   ```
6. Open browser and your app located in http://localhost:8080
7. Login with user "admin" password "persiapanreuniakbar2022"

# Log Progress
Catatan pengembangan

## Database schema
![Database schema](https://snz04pap001files.storage.live.com/y4mXEraX9NbUV8YqO8Pk45l7KS2ofa_zH3S7qW0MYs8eeRHhMlNQVsUtztON3FkoggUkHhOEBNczw-woIBETdzFr-mCXBMQQ-H1UGzMz10qw1nINz6ZnXTK0bO8_-JqdTi-pqZ59GMh-abUgMMTT4rxasu3YPsWb7zbi4TFKgq3r1rxhy9ghj8VNkyUylVKFe7y?width=985&height=1322&cropmode=none)

### Migration

1.  Pekerjaan
    ```bash
    yii migrate/create create_pekerjaan_table --fields="nama:string:notNull,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

2.  Profile
    ```bash
    yii migrate/create create_profile_table --fields="id_user:integer:foreignKey(user),nama:string:notNull,alamat:string:notNull,no_hp:string:notNull,foto:string,id_pekerjaan:integer:foreignKey(pekerjaan),pekerjaan_lainnya:string,lulus_tahun:integer:notNull,status_anggota:integer:defaultValue(9),created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

3.  Event
    ```bash
    yii migrate/create create_event_table --fields="owner:integer:notNull:foreignKey(user),nama:string:notNull,deskripsi:text,tanggal_mulai:dateTime:notNull,tanggal_selesai:dateTime:notNull,event_seharian:boolean:defaultValue(0),tempat:string:notNull,alamat:string,foto:string,total_tiket:integer,tiket_unlimited:boolean:defaultValue(0),created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

4.  Tiket  Kategori Harga
    ```bash
    yii migrate/create create_tiket_kategori_harga_table --fields="id_event:integer:notNull:foreignKey(event),nama:string:notNull,harga:integer:notNull,total_tiket:integer,tiket_tersisa:integer,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

5.  Event Setting
    ```bash
    yii migrate/create create_event_setting_table --fields="id_event:integer:notNull:foreignKey(event),tampilkan_tiket_tersisa:boolean:defaultValue(0),tampilkan_tiket_tersisa_kategori:boolean:defaultValue(0),tampilkan_tiket_terjual:boolean:defaultValue(1),maksimal_pembelian_tiket:integer:notNull:defaultValue(10),peta_lokasi_long:decimal,peta_lokasi_lat:decimal,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

6.  Event Coupon
    ```bash
    yii migrate/create create_event_coupon_table --fields="id_event:integer:notNull:foreignKey(event),keyword:string:notNull,diskon:integer,tanggal_mulai:dateTime,tanggal_selesai:dateTime,jumlah_maksimal:integer,sisa:integer,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```bash

7.  Beli Event
    ```bash
    yii migrate/create create_beli_event_table --fields="id_event:integer:notNull:foreignKey(event),kode_beli:string:notNull,qty_tiket:integer:notNull,total_bayar:bigInteger:notNull,status_bayar:integer:defaultValue(9),created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

8.  Beli Event Detail
    ```bash
    yii migrate/create create_beli_event_detail_table --fields="id_beli_event:integer:notNull:foreignKey(beli_event),id_tiket_kategori_harga:integer:notNull:foreignKey(tiket_kategori_harga),id_event_coupon:integer:foreignKey(event_coupon),harga:bigInteger:notNull,jumlah_tiket:integer:notNull,total_harga:bigInteger,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

9.  Tiket  Cetak
    ```bash
    yii migrate/create create_tiket_cetak_table --fields="id_event:integer:notNull:foreignKey(event),id_beli_event:integer:foreignKey(beli_event),kode_tiket:string:notNull:unique,keterangan:string,sudah_digunakan:boolean:notNull:defaultValue(0),created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

10. Event Panitia
    ```bash
    yii migrate/create create_event_panitia_table --fields="id_event:integer:notNull:foreignKey(event),id_user:integer:notNull:foreignKey(user),created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

11. Log
    ```bash
    yii migrate/create create_log_table --fields="deskripsi:text:notNull,link_url:string,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

12. Beli Event History Status
    ```bash
    yii migrate/create create_beli_event_history_status_table --fields="id_beli_event:integer:foreignKey(beli_event),status_bayar:integer,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

13. Kategori
    ```bash
    yii migrate/create create_kategori_table --fields="nama:string:notNull,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

14. Produk
    ```bash
    yii migrate/create create_produk_table --fields="kode_sku:string:notNull:unique,id_kategori:integer:notNull:foreignKey(kategori),nama:string:notNull,harga:bigInteger:notNull,stok:integer,stok_unlimited:boolean:defaultValue(0),pre_order:boolean:defaultValue(0),foto:string,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

15. Variasi
    ```bash
    yii migrate/create create_variasi_table --fields="nama:string:notNull,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

16. Variasi Nilai
    ```bash
    yii migrate/create create_variasi_nilai_table --fields="id_variasi:integer:notNull:foreignKey(variasi),nilai:string:notNull,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

17. Produk  Variasi Nilai
    ```bash
    yii migrate/create create_produk_variasi_nilai_table --fields="kode_sku:string:notNull:unique,id_produk:integer:notNull:foreignKey(produk),id_variasi_nilai:integer:notNull:foreignKey(variasi_nilai),harga:bigInteger:notNull,stok:integer:notNull,stok_unlimited:boolean:notNull:defaultValue(0),pre_order:boolean:defaultValue(0),foto:string,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

18. Beli Produk
    ```bash
    yii migrate/create create_beli_produk_table --fields="kode_beli:string:notNull:unique,tanggal_beli:dateTime:notNull,qty_produk:integer:notNull,total_bayar:bigInteger:notNull,status_bayar:integer:notNull,status_beli:integer,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```

19. Beli Produk Detail
    ```bash    
    yii migrate/create create_beli_produk_detail_table --fields="id_beli_produk:integer:notNull:foreignKey(beli_produk),id_produk_variasi_nilai:integer:foreignKey(produk_variasi_nilai),id_produk:integer:foreignKey(produk),harga:bigInteger:notNull,jumlah:integer:notNull,total_harga:bigInteger:notNull,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```
20. Beli Produk History Status
    ```bash
    yii migrate/create create_beli_produk_history_status_table --fields="id_beli_produk:integer:notNull:foreignKey(beli_produk),status_beli:integer,created_by:integer:foreignKey(user),updated_by:integer:foreignKey(user),created_at:dateTime,updated_at:dateTime"
    ```
## TODO
1. ~~Membuat migration~~
2. Integrasi template
3. Membuat register
4. Mengisi profile
5. Membuat dashboard
   1. widget registered user
   2. widget ticket revenue
   3. widget product revenue
   4. widget reports (sales num, revenue num, customer num)
   5. recent registered user
   6. recent product sales
   7. recent ticket sales
6.  